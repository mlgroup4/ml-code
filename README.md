## Short Summary- This is same as the abstract of our final report

Botnet attacks have a serious impact on businesses and result in huge losses to many organizations 
around the world. Internet-of-things devices are prone to these attacks the most and result serious 
damages which are worse than computer based attacks. In this paper, we developed an anomaly detection 
model to identified malicious network traffic emanating from IoT devices which helps in identifying whether a device is 
infected with botnets. The Anomaly Detector is developed in three ways using Multivariate Gaussian, Deep Autoencoder and 
Supervised learning. The performance and computational complexity of all the three anomaly detection models are compared 
to pick the most suitable one for real time applications. Once the anomalies are detected, these are further classified 
into type of botnet attacks- Mirai or Bashlite(gafgyt) by building a classifier on the output of the anomaly detector. 
The anomaly detection model can be used for commercial purposes. The model which has classifier built on the output of 
anomaly detector can be used for scientific  research and analysis of botnet attacks. 
---

## DataSet Link
https://archive.ics.uci.edu/ml/datasets/detection_of_IoT_botnet_attacks_N_BaIoT

## File Description

1. Join_Benign_Data.py joins the benign datasets of all the 9 IoT devices.
2. Join_Malicious_Data.py joins the malicious datasets of all the 9 IoT devices.
3. Fisher_score_calculator.py calculates fisher's score across all the features.
4. Gaussian_final.py is a gaussian model developed for anomaly detection
5. Autoencoder_final.py is a deep autoencoder model developed for anomaly detection
6. Supervised_anomaly_detection_final.py is supervised learning model developed for anomaly detection.
7. Classifier_built_on_the_anomaly_detector.py is a meta model, which has a classifier built on the output of the anomaly detector. It helps in classifying botnet attacks into mirai and bashlite. 