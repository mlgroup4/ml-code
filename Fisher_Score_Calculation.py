import pandas as pd
from skfeature.function.similarity_based import fisher_score

top_n_features = 115
#importing the data set
df_benign = pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_benign.csv')
df_benign = df_benign.sample(n=1000, random_state=17)
df_malicious = pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_malicious.csv')
df_malicious = df_malicious.sample(n=1000, random_state=17)
#Creating the tags
df_benign['Anomaly'] = 0
df_malicious['Anomaly'] = 1
df = df_benign.append(df_malicious)
Y = df['Anomaly']
X = df.drop(columns=['Anomaly']).values
score = fisher_score.fisher_score(X,Y)
score = pd.DataFrame(score)
idx = fisher_score.feature_ranking(score)
idx = pd.DataFrame(idx)
selected_features = X[:, idx[0:top_n_features]]
fisher = pd.concat([score, idx], axis =1)
print(fisher)
