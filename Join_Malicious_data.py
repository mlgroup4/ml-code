from glob import iglob
import pandas as pd

df_mirai = pd.concat((pd.read_csv(f) for f in iglob('C:/Users/mouni/.spyder-py3/ML/data/**/mirai_attacks/*.csv', recursive=True)), ignore_index=True)
df_mirai.to_csv("merged_mirai.csv", index = False)
df_gafgyt = pd.DataFrame()
for f in iglob('C:/Users/mouni/.spyder-py3/ML/data/**/gafgyt_attacks/*.csv', recursive=True):
    df_gafgyt = df_gafgyt.append(pd.read_csv(f), ignore_index=True)
df_gafgyt.to_csv("merged_gafgyt.csv", index = False)
df = df_mirai.append(df_gafgyt)
df.to_csv("merged_malicious.csv", index = False)
