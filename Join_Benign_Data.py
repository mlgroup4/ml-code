import glob
import os
import pandas as pd

filepath = r'C://Users/mouni/.spyder-py3/ML/data/**/benign_traffic.csv'
comb  = glob.glob(os.path.join(filepath))
df_individualfiles = (pd.read_csv(f) for f in comb)
df = pd.concat(df_individualfiles, ignore_index = True)
df.to_csv("merged_benign.csv", index = False)
