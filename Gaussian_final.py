import time
start = time.time()
import psutil
import os
process = psutil.Process(os.getpid())
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from scipy.stats import multivariate_normal
from sklearn.metrics import recall_score, accuracy_score, precision_score, f1_score, confusion_matrix

def gauss(data):
    mu = np.mean(data, axis = 0)
    sigma = np.cov(data.T)
    return mu, sigma

def multivariate_gaussian(data, mu, sigma):
    p = multivariate_normal(mean = mu, cov = sigma, allow_singular = True)
    return p.pdf(data)

def threshold(p, Y_test):
    best_e = 0
    best_f1 = 0
    f = 0
    step_size = (max(p) - min(p)) / 1000
    e = np.arange(min(p), max(p), step_size)
    for e in np.nditer(e):
        pred = (p < e)
        f = f1_score(Y_test, pred, average = "binary")
        if f > best_f1:
            best_f1 = f
            best_e = e
    return best_f1, best_e
# Reading the data from the location and concat them to single dataframe
data =pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_benign.csv')
#splitting the data into 3 equal parts train,validation and test 
x_train, x_cv, x_test = np.split(data.sample(frac=1, random_state=17), 
                                 [int(1/3*len(data)), int(2/3*len(data))])
#Using the standar scaler to normalise the data
scaler = StandardScaler()
x_train = scaler.fit_transform(x_train)
x_cv = scaler.fit_transform(x_cv)
mu, sigma = gauss(x_train)

#test data   
#load malicious dataset
df_malicious = pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_malicious.csv')
df_malicious = df_malicious.sample(n = x_test.shape[0], random_state=17)
df_benign = pd.DataFrame(x_test, columns=data.columns)
df_benign['malicious'] = 0
df_malicious['malicious'] = 1
df = df_benign.append(df_malicious)
X_test = df.drop(columns=['malicious']).values
X_test_scaled = scaler.transform(X_test)
Y_test = df['malicious']
p_cv = multivariate_gaussian(x_cv, mu, sigma)
f1score, epsilon = threshold(p_cv, Y_test)
end = time.time()
print("Time taken for training:", end - start)
print("best f1_score is:", f1score)
print("best epsilon is:", epsilon)
start1 = time.time()
p_test = multivariate_gaussian(X_test_scaled, mu, sigma)
Y_pred = (p_test < epsilon)
print('Accuracy')
print(accuracy_score(Y_test, Y_pred))
print('Recall')
print(recall_score(Y_test, Y_pred))
print('Precision')
print(precision_score(Y_test, Y_pred))
print('f1_score')
print(f1_score(Y_test, Y_pred))
cm = confusion_matrix(Y_test, Y_pred)
print(cm)
end1 = time.time()
print("time taken for testing:", end1-start1)
print("memory taken for gaussian model:", process.memory_info().rss)
            
        
        
    