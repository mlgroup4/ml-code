import time
start = time.time()
import psutil
import os
process = psutil.Process(os.getpid())
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier as RFC
from sklearn.metrics import recall_score, accuracy_score, precision_score, confusion_matrix, f1_score

# Reading the data from the location and concat them to single dataframe
df_benign =pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_benign.csv')
df_gafgyt = pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_gafgyt.csv')
df_mirai = pd.read_csv('file:///C:/Users/mouni/.spyder-py3/ML/merged_mirai.csv')
df_gafgyt = df_gafgyt.sample(n=df_benign.shape[0], random_state=17)
df_mirai = df_mirai.sample(n=df_benign.shape[0], random_state=17)
df_benign['Botnet_Type'] = 0
df_benign['Anomaly'] = 0
df_gafgyt['Botnet_Type'] = 1
df_mirai['Botnet_Type'] = 2
df_malicious = df_gafgyt.append(df_mirai)
df_malicious = df_malicious.sample(n=df_benign.shape[0], random_state=17)
df_malicious['Anomaly'] = 1
df = df_benign.append(df_malicious)
Y = df['Anomaly']
array = df.values
X = array[:, 0:116]
x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size = 0.2)
#scale data for efficiency
scaler =  StandardScaler()
x_train =  pd.DataFrame(x_train)
x_test =  pd.DataFrame(x_test)
x_train = scaler.fit_transform(x_train.iloc[:, 0:115])
clf = RFC()
clf.fit(x_train, y_train)
#test the model on data
x_test_scaled = scaler.transform(x_test.iloc[:, 0:115])
Y_pred = clf.predict(x_test_scaled)
print('Accuracy')
print(accuracy_score(y_test, Y_pred))
print('Recall')
print(recall_score(y_test, Y_pred))
print('Precision')
print(precision_score(y_test, Y_pred))
print('f1_score')
print(f1_score(y_test, Y_pred))
cm = confusion_matrix(y_test, Y_pred)
print(cm)
x_test = pd.DataFrame(x_test)
x_test['Predicted_result'] = Y_pred
df_botnet_class = x_test.loc[x_test['Predicted_result'] ==1]
array1 = df_botnet_class.values
X_class = array1[:, 0:115]
Y_class = df_botnet_class.iloc[:,115]
x_train, x_test, y_train, y_test = train_test_split(X_class,Y_class, test_size = 0.2)
#scale data for efficiency
scaler =  StandardScaler()
x_train = scaler.fit_transform(x_train)
metaclf = RFC()
metaclf.fit(x_train, y_train)
#test the model on data
x_test = scaler.transform(x_test)
Y_pred = metaclf.predict(x_test)
print('Accuracy')
print(accuracy_score(y_test, Y_pred))
print('Recall')
print(recall_score(y_test, Y_pred))
print('Precision')
print(precision_score(y_test, Y_pred))
print('f1_score')
print(f1_score(y_test, Y_pred))
cm = confusion_matrix(y_test, Y_pred)
print(cm)
end = time.time()
print("time consumed for training:", end-start)
print("memory taken for supervised anomaly detection and anomaly classification:", process.memory_info().rss)